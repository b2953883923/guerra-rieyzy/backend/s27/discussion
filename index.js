

// [SECTION] Array Methods.

	// JavaScript has build in functions and methods of array. This allows us to manipulate and access array elements.

	// Mutator Methods 

	/*
		- Mutator methods are functions that "mutate" or change an array after it is created
		- The methods manipulate the original array performing various tasks such as adding and/or removing elements.
	*/

let fruits = ["apple", "orange", "kiwi", "dragon fruit"];
console.log("Original Arrayy");
console.log(fruits);

// push();

	// Add an element at the end of an array and returns the array's length

	/*
		Syntax:
			arrayName.push("itemToPush");
	*/

	let fruitsLength = fruits.push("mango");
	console.log(fruitsLength);
	console.log("Mutated array using method: ");
	console.log(fruits);

	// Adding Multiple Element's to an array
	fruits.push("avocado", "guava");
	console.log("Mutated array using method: ");
	console.log(fruits);

	// Let's try it in a function
	function addFruit(fruit){
		fruits.push(fruit);
	}
	addFruit("pinya");
	console.log("Mutated array using push method: ");
	console.log(fruits);

// pop()

	// removes the last element in the array and returns the removed element.

	/*
		Syntax:
			arrayName.pop();
	*/ 
	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log("Mutated array using pop method: ");
	console.log(fruits);

	// Lets try it in a function

	function removeFruit(){
		fruits.pop();
	}
	removeFruit()
	console.log("Mutated array using pop method: ");
	console.log(fruits);

// unshift()
	// Adds one or more element/s at the beginning of the array

	/*
		Syntax:
		arrayName.unshift("elementA");
		arrayName.unshift("elementA","elementB");
	*/ 

	fruits.unshift("lime", "banana");
	console.log("Mutated array using unshift method: ");
	console.log(fruits);

	function unshiftFruit(fruit){
		fruits.unshift(fruit);

	}
	unshiftFruit("strawberry");
	console.log("Mutated array using unshift method: ");
	console.log(fruits);

//shift()

	//removes an element at the beginning of an array and returns the remove element.

	/*
		Syntax:
			arrayName.shift();
	*/ 

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array using shift method: ");
	console.log(fruits);

	// Try in a function
	function shiftFruit(){
		fruits.shift();
	}
	fruits.shift()
	console.log("Mutated array using shift method: ");
	console.log(fruits);

// splice()

	//Simultaneously removes elements from a specified index number and adds elements.

	/*
		Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

	*/ 
	//start in index 1, delete 2 element, replace with new.
	fruits.splice(1, 2, "jackfruit", "cherry"); 
	console.log("Mutated array using splice method: ");
	console.log(fruits);

	function spliceFruit(index, deleteCount, fruit){
		fruits.splice(index,deleteCount,fruit);
	}

	spliceFruit(1,1,"rambutan");
	console.log("Mutated array using splice method: ");
	console.log(fruits);

// sort()
	// Rearranges the array elements in alphanumeric order.
	/*
		Syntax:
		arrayName.sort();
	*/ 

	// fruits.sort();
	// console.log("Mutated array using sort method: ");
	// console.log(fruits);

	// Try in function
	function sortFruit(){
		fruits.sort();
	}
	sortFruit();
	console.log("Mutated array using sort method: ");
	console.log(fruits);

	let mixedArr = [100, 10, "apple" , 1000, "butter" , "Chocolate", "Cheese", 99];
	console.log("Mutated array using sort method: ");
	console.log(mixedArr);

// reverse()
	// Reverse the order of the array elements

	/*
		Syntax:
			arrayName.reverse();
	*/ 

	fruits.reverse();
	console.log("Mutated array using reverse method: ");
	console.log(fruits);

	// try in function
	function reverseFruit(){
		fruits.reverse();
	}

	reverseFruit()
	console.log("Mutated array using reverse method: ");
	console.log(fruits);

	mixedArr.reverse()
	console.log("Mutated array using reverse method: ");
	console.log(mixedArr);